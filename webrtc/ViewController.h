//
//  ViewController.h
//  webrtc
//
//  Created by luyao on 17/3/15.
//  Copyright © 2017年 reconova. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Toast.h"
#import "webrtcclient.h"

@interface ViewController : UIViewController<UITextFieldDelegate, RecoLiveEventDelegate>
@property (strong, nonatomic) IBOutlet UITextField *signalServerAddress;
- (IBAction)loginServer:(id)sender;
- (IBAction)logoutServer:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *peerNameTextField;
- (IBAction)hangupPeer:(id)sender;
- (IBAction)callPeer:(id)sender;

- (IBAction)lowResolution:(id)sender;
- (IBAction)middleResolution:(id)sender;
- (IBAction)highResolution:(id)sender;


- (IBAction)downloadFile:(id)sender;
- (IBAction)cancelDownload:(id)sender;
- (IBAction)removeFile:(id)sender;


- (IBAction)queryFile:(id)sender;
- (IBAction)replayVideo:(id)sender;
- (IBAction)stopReplay:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *remoteVideo;
@property (weak, nonatomic) IBOutlet UILabel *downloadFileInfo;
@end

