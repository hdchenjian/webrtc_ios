#import <Foundation/Foundation.h>
#import "SRWebSocket.h"

#import "WebRTC/RTCEAGLVideoView.h"
#import "WebRTC/RTCMediaStream.h"
#import "WebRTC/RTCPeerConnectionFactory.h"
#import "WebRTC/RTCMediaConstraints.h"
#import "WebRTC/RTCPeerConnection.h"
#import "WebRTC/RTCVideoTrack.h"
#import "WebRTC/RTCAudioTrack.h"
#import "WebRTC/RTCICECandidate.h"
#import "WebRTC/RTCIceServer.h"
#import "WebRTC/RTCConfiguration.h"
#import "WebRTC/RTCSessionDescription.h"
#import "WebRTC/RTCDataChannel.h"
#import "WebRTC/RTCDataChannelConfiguration.h"

@interface PeerConnectionParameters : NSObject
@property (nonatomic) BOOL videoCodecHwAcceleration;
@property (nonatomic) NSInteger videoFps;
@property (nonatomic) BOOL useVideoChannel;
@end

@protocol RecoLiveEventDelegate <NSObject>

//接收到视频流
- (void)onAddRemoteStream:(RTCVideoTrack* )remoteStream;
//视频流中断
- (void)onRemoveRemoteStream;
//通知用户的出错消息
- (void)onMessage:(NSString *)message;
//登陆服务器
-(void)onLogin;
// 登出
-(void)onLogout;
//连接到采集端
-(void)onConnect;

//采集端离线
-(void)onOffLine;
//连接断开
-(void)onDisconnected;
//采集端发送notify消息
-(void)onNotify:(NSString *)message;

//采集端接受文件传输请求
-(void)onFileAccept;
//采集端拒绝文件传输请求
-(void)onFileReject;
//文件传输进度
-(void)onFileProgress:(NSString *)data;
//文件传输完成
-(void)onFileFinish:(NSString *)data;
//文件传输取消
-(void)onFileAbort:(NSString *)data;
//请求传输的文件大小为0
-(void)onFileSizeError:(NSString *)data;

@end

@interface webrtcClient : NSObject
- (instancetype)initWithDelegate:(id<RecoLiveEventDelegate>)delegate;
- (BOOL)hasCallInit;
- (BOOL)isConnected;
- (BOOL)isLogined;
- (void)initWebsocket:(NSString* )socketAddress hostIP:(NSString* )hostIP peerName:(NSString* )peerNameString;
- (void)uninitWebsocket;
- (void)startWebrtc:(PeerConnectionParameters* )params peerName:(NSString* )peerNameString;
- (void)stopWebrtc;
- (void)sendMessage:(NSString* )message;
- (NSInteger)requestGetFile:(NSString* )fileName savePath:(NSString* ) savePath;
- (void)cancelFileDownload;
@end
