//
//  ViewController.m
//  webrtc
//
//  Created by luyao on 17-3-6.
//  Copyright (c) 2017年 reconova. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

#import "ViewController.h"
//#import "ARDVideoCallView.h"

@interface ViewController () <RTCEAGLVideoViewDelegate>
@property(nonatomic, readonly) RTCEAGLVideoView *remoteVideoView;
@property(nonatomic, strong) RTCVideoTrack *remoteVideoTrack;
@end

@implementation ViewController {
    webrtcClient* client;
    CGSize _remoteVideoSize;
}
@synthesize remoteVideoView = _remoteVideoView;
@synthesize remoteVideoTrack = _remoteVideoTrack;
@synthesize downloadFileInfo = _downloadFileInfo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.peerNameTextField setDelegate:self];
    [self.signalServerAddress setDelegate:self];
    self.signalServerAddress.tag = 0;
    self.peerNameTextField.tag = 1;

    client = [[webrtcClient alloc] initWithDelegate:self];
    //[self.remoteView setDelegate:self];
    _remoteVideoView = [[RTCEAGLVideoView alloc] initWithFrame:self.remoteVideo.bounds];
    _remoteVideoView.delegate = self;
    _remoteVideoView.transform = CGAffineTransformMakeScale(-1, 1);
    [self.remoteVideo addSubview:_remoteVideoView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.signalServerAddress becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"You entered %@",self.peerNameTextField.text);
    if(textField.tag == 0) {
        [self.signalServerAddress resignFirstResponder];
    } else {
        [self.peerNameTextField resignFirstResponder];
    }
    return YES;
}

- (BOOL)toastLoginUnSuccess {
    if (![client isLogined]) {
        [self toastMessage:@"尚未登陆成功!"];
        return YES;
    } else {
        return NO;
    }
}

- (void)recoLiveInit:(NSString *)serverAddress {
    NSString *signalServerAddress = serverAddress;
    NSArray* host_port = [signalServerAddress componentsSeparatedByString:@":"];
    NSString* ip;
    NSString* port;
    if([host_port count] == 2) {
        ip = host_port[0];
        port = host_port[1];
    } else {
        NSLog(@"server address format error");
        return;
    }
    signalServerAddress = [NSString stringWithFormat:@"%@%@%@%@%@", @"ws://", ip, @":", port, @"/"];
    NSLog(@"connect to %@", signalServerAddress);

    NSString *peerName = self.peerNameTextField.text;
    [client initWebsocket:signalServerAddress hostIP:ip peerName:peerName];
}

- (void)recoLiveSart:(BOOL)useVideoChannel_ {
    if([client hasCallInit]) {
        return;
    } else {
        NSString *peerName = self.peerNameTextField.text;
        PeerConnectionParameters* params = [[PeerConnectionParameters alloc] init];
        params.videoCodecHwAcceleration = YES;
        params.videoFps = 15;
        params.useVideoChannel = useVideoChannel_;
        NSLog(@"recoLiveSart: call %@: %d", peerName, params.useVideoChannel);
        [client startWebrtc:params peerName:peerName];
    }
}

- (void)recoLiveUnInit:(BOOL)cleanAll {
    if (_remoteVideoTrack != nil) {
        [_remoteVideoTrack removeRenderer:_remoteVideoView];
        _remoteVideoTrack = nil;
        [_remoteVideoView renderFrame:nil];
    }
    [client stopWebrtc];
    if (cleanAll) {
        [client uninitWebsocket];
    }
}

- (IBAction)loginServer:(id)sender {
    if(client.isLogined) {
        [self toastMessage:@"您已经登录了!"];
        return;
    }
    NSString *signalServerAddress = self.signalServerAddress.text;
    signalServerAddress = @"p2p0.reconova.com:3000";
    [self recoLiveInit:signalServerAddress];
}

- (IBAction)logoutServer:(id)sender {
    if([self toastLoginUnSuccess]) {
        return;
    }
    [self recoLiveUnInit:YES];
}

- (IBAction)callPeer:(id)sender {
    if([self toastLoginUnSuccess]) {
        return;
    }
    [self recoLiveSart:YES];
    [client sendMessage:@"start_video_live"];
}

- (IBAction)hangupPeer:(id)sender {
    if([self toastLoginUnSuccess]) {
        return;
    }
    [client sendMessage:@"stop_video_live"];
    [self recoLiveUnInit:NO];
}


- (void)toastMessage:(NSString *)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view makeToast:message];
    });
}

// implement RecoLiveEventDelegate
- (void) onAddRemoteStream:(RTCVideoTrack* )track {
    NSLog(@"onAddRemoteStream");
    _remoteVideoTrack = track;
    [_remoteVideoTrack addRenderer:_remoteVideoView];
}

- (void) onRemoveRemoteStream {
    NSLog(@"onRemoveRemoteStream");
}

- (void)onMessage:(NSString *)message {
    [self toastMessage:message];
}

-(void)onLogin{
    NSLog(@"onLogin");
}

-(void)onLogout{
    NSLog(@"onLogout");
}

-(void)onConnect{
    NSLog(@"onConnect");
}

-(void)onOffLine{
    NSLog(@"onOffLine");
}

-(void)onDisconnected{
    NSLog(@"onDisconnected");
    NSLog(@"Peer Leave");
    [self recoLiveUnInit:NO];
}

-(void)onNotify:(NSString *)message{
    NSLog(@"onNotify: %@", message);
}

-(void)onFileAccept{
    NSLog(@"onFileAccept");
}

-(void)onFileReject{
    NSLog(@"onFileReject");
}

-(void)onFileProgress:(NSString *)data{
    NSLog(@"onFileProgress: %@", data);
}

-(void)onFileFinish:(NSString *)data{
    NSLog(@"onFileFinish: %@", data);
}

-(void)onFileAbort:(NSString *)data{
    NSLog(@"onFileAbort: %@", data);
}

-(void)onFileSizeError:(NSString *)data {
    NSLog(@"onFileSizeError: %@", data);
}

// RTCEAGLVideoViewDelegate
- (void)videoView:(RTCEAGLVideoView *)videoView didChangeVideoSize:(CGSize)size {
    if (videoView == _remoteVideoView) {
    _remoteVideoSize = size;
  }
  [self updateVideoViewLayout];
}

- (void)updateVideoViewLayout {
    NSLog(@"updateVideoViewLayout");
    CGSize defaultAspectRatio = CGSizeMake(4, 3);
    CGSize remoteAspectRatio = CGSizeEqualToSize(_remoteVideoSize, CGSizeZero) ?
        defaultAspectRatio : _remoteVideoSize;

    CGRect remoteVideoFrame =
        AVMakeRectWithAspectRatioInsideRect(remoteAspectRatio, self.remoteVideo.bounds);
    self.remoteVideoView.frame = remoteVideoFrame;
}

- (IBAction)lowResolution:(id)sender {
    [client sendMessage:[NSString stringWithFormat:@"change_resolution?width=%d&height=%d", 320, 240]];
}

- (IBAction)middleResolution:(id)sender {
    [client sendMessage:[NSString stringWithFormat:@"change_resolution?width=%d&height=%d", 640, 480]];
}
- (IBAction)highResolution:(id)sender {
    [client sendMessage:[NSString stringWithFormat:@"change_resolution?width=%d&height=%d", 1280, 720]];
}

- (IBAction)downloadFile:(id)sender {
    [self recoLiveSart:NO];
    NSString *fileName = @"2_2017-02-28_19-39-56_T.mp4";
    NSString *savePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    _downloadFileInfo.text = [NSString stringWithFormat:@"文件存储位置: %@\n共: %d字节, 已下载: %d字节\n进度: %d",
                              savePath, 0, 0, 0];
    [client requestGetFile:fileName savePath:savePath];
}

- (IBAction)cancelDownload:(id)sender {
    _downloadFileInfo.text = @"已取消下载";
    [client cancelFileDownload];
}

- (IBAction)removeFile:(id)sender {
    NSString *fileName = @"";
    [client sendMessage:[NSString stringWithFormat:@"delete_file?filename=%@", fileName]];
}

- (IBAction)queryFile:(id)sender {
    [client sendMessage:[NSString stringWithFormat:@"file_query?top=%d&skip=%d&type=%@", 1, 0, @"video_all"]];
}

- (IBAction)replayVideo:(id)sender {
    [self recoLiveSart:YES];
    [client sendMessage:[NSString stringWithFormat:@"video_replay?video=%@", @"2_2017-02-28_19-39-56_T.mp4"]];
}

- (IBAction)stopReplay:(id)sender {
    [client sendMessage:[NSString stringWithFormat:@"stop_video_replay?video=%@", @"2_2017-02-28_19-39-56_T.mp4"]];
}
@end
