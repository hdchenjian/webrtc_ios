#import <CommonCrypto/CommonHMAC.h>
#import "webrtcclient.h"

//Class webrtcClient;

@interface webrtcClient () <SRWebSocketDelegate>
@property(nonatomic) RTCDataChannel* dataChannelLocal;
@property(nonatomic) BOOL isDownloadFile;

- (void)sendMessage:(NSString* )type payload:(NSMutableDictionary* )payload;

@end

@interface Peer : NSObject <RTCPeerConnectionDelegate, RTCDataChannelDelegate>
@property(nonatomic) NSInteger fileSizeCurrent;
@property(nonatomic) NSInteger fileSize;
@property(nonatomic) NSString* savePath;
@property(nonatomic) NSString* fileName;
@property(nonatomic) BOOL videoChannelUsed;

- (instancetype)initWithDelegate:(id<RecoLiveEventDelegate>)delegate_ creator:(webrtcClient*)creator;
- (void)cleanDataSaveParameter;
@end

@implementation Peer {
    id<RecoLiveEventDelegate> delegate;
    webrtcClient* parent;
    NSString* realFileSavePath;
    dispatch_queue_t queueDataChannel;
}

@synthesize fileSizeCurrent = _fileSizeCurrent;
@synthesize fileSize = _fileSize;
@synthesize savePath = _savePath;
@synthesize fileName = _fileName;
@synthesize videoChannelUsed = _videoChannelUsed;

- (instancetype)initWithDelegate:(id<RecoLiveEventDelegate>)delegate_ creator:(webrtcClient*)creator {
    if (self = [super init]) {
        delegate = delegate_;
        parent = creator;
        _fileSizeCurrent = 0;
        _fileSize = 0;
        _savePath = @"";
        _fileName = @"";
        realFileSavePath = @"";
    }
    queueDataChannel = dispatch_queue_create("webrtc.dataChannelTaskQueue", DISPATCH_QUEUE_SERIAL);
    return self;
}

- (void)cleanDataSaveParameter {
    _fileSizeCurrent = 0;
    _fileSize = 0;
    _savePath = @"";
    _fileName = @"";
    realFileSavePath = @"";
}

// implement RTCDataChannelDelegate
/** The data channel state changed. */
- (void)dataChannelDidChangeState:(RTCDataChannel *)dataChannel {
    NSLog(@"dataChannelDidChangeState: %ld", parent.dataChannelLocal.readyState);
}

/** The data channel successfully received a data buffer. */
- (void)dataChannel:(RTCDataChannel *)dataChannel
didReceiveMessageWithBuffer:(RTCDataBuffer *)buffer {
    NSLog(@"didReceiveMessageWithBuffer");
    [self saveData:buffer.data];
}

/** The data channel's |bufferedAmount| changed. */
- (void)dataChannel:(RTCDataChannel *)dataChannel
didChangeBufferedAmount:(uint64_t)amount {
    NSLog(@"didChangeBufferedAmount: %lld", amount);
}


// implement RTCPeerConnectionDelegate
/** Called when the SignalingState changed. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
didChangeSignalingState:(RTCSignalingState)stateChanged{
    NSLog(@"Signaling state changed: %ld", (long)stateChanged);
}

/** Called when media is received on a new stream from remote peer. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
          didAddStream:(RTCMediaStream *)stream{
    NSLog(@"Received %lu video tracks and %lu audio tracks",
          (unsigned long)stream.videoTracks.count,
          (unsigned long)stream.audioTracks.count);
    NSLog(@"%d", _videoChannelUsed);
    dispatch_async(dispatch_get_main_queue(), ^{
            if (stream.videoTracks.count && _videoChannelUsed) {
                RTCVideoTrack *videoTrack = stream.videoTracks[0];
                [delegate onAddRemoteStream:videoTrack];
            }
        });
}

/** Called when a remote peer closes a stream. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
       didRemoveStream:(RTCMediaStream *)stream{
    NSLog(@"didRemoveStream");
    dispatch_async(dispatch_get_main_queue(), ^{
            [delegate onRemoveRemoteStream];
        });
}

/** Called when negotiation is needed, for example ICE has restarted. */
- (void)peerConnectionShouldNegotiate:(RTCPeerConnection *)peerConnection{
    NSLog(@"WARNING: Renegotiation needed but unimplemented.");
}

/** Called any time the IceConnectionState changes. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
didChangeIceConnectionState:(RTCIceConnectionState)newState{
    NSLog(@"ICE state changed: %ld", (long)newState);
    dispatch_async(dispatch_get_main_queue(), ^{
        if(newState == RTCIceConnectionStateDisconnected) {
            [delegate onDisconnected];
        }
        });
}

/** Called any time the IceGatheringState changes. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
didChangeIceGatheringState:(RTCIceGatheringState)newState{
    NSLog(@"ICE gathering state changed: %ld", (long)newState);
}

/** New ice candidate has been found. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
didGenerateIceCandidate:(RTCIceCandidate *)candidate{
    dispatch_async(dispatch_get_main_queue(), ^{
            NSMutableDictionary* payload = [NSMutableDictionary dictionaryWithCapacity:3];
            [payload setObject:[NSNumber numberWithInt:candidate.sdpMLineIndex] forKey:@"sdpMLineIndex"];
            [payload setObject:candidate.sdpMid forKey:@"sdpMid"];
            [payload setObject:candidate.sdp forKey:@"candidate"];
            [parent sendMessage:@"candidate" payload:payload];
        });
}

/** Called when a group of local Ice candidates have been removed. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
didRemoveIceCandidates:(NSArray<RTCIceCandidate *> *)candidates{}

/** New data channel has been opened. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
    didOpenDataChannel:(RTCDataChannel *)dataChannel{
    NSLog(@"didOpenDataChannel");
    parent.dataChannelLocal = dataChannel;
}

- (void)saveData:(NSData *)data_content {
    dispatch_async(queueDataChannel, ^{
            NSArray *filePathSplit = [_fileName componentsSeparatedByString:@"."];
            NSArray *fileNameExtend;
            if([filePathSplit count] == 2) {
                fileNameExtend = filePathSplit;
            } else {
                fileNameExtend = [NSArray arrayWithObjects: _fileName, @"", nil];
            }
            if (_fileSizeCurrent == 0) {
                NSFileManager *fm = [NSFileManager defaultManager];
                realFileSavePath = [NSString stringWithFormat:@"%@/%@", _savePath,  _fileName];
                int i = 0;
                while([fm fileExistsAtPath:realFileSavePath] == YES){
                    realFileSavePath = [NSString stringWithFormat:@"%@/%@_%d.%@", _savePath,
                                                 [fileNameExtend objectAtIndex:0], i,
                                                 [fileNameExtend objectAtIndex:1]];
                    i += 1;
                    NSLog(@"%@", realFileSavePath);
                }
                NSLog(@"%@", realFileSavePath);
                [fm createFileAtPath:realFileSavePath contents:nil attributes:nil];
            }
            NSFileHandle *file = [NSFileHandle fileHandleForUpdatingAtPath:realFileSavePath];
            [file seekToEndOfFile];
            [file writeData:data_content];
            [file closeFile];
            _fileSizeCurrent += data_content.length;
            NSLog(@"saveData:fileSizeCurrent %ld", _fileSizeCurrent);
            //mRecoLiveEvent.onFileProgress(realFileSavePath, fileSize, fileSizeCurrent);
            if (_fileSizeCurrent >= _fileSize) {
                //mRecoLiveEvent.onFileFinish();
                NSMutableDictionary* message = [NSMutableDictionary dictionaryWithCapacity:3];
                dispatch_async(dispatch_get_main_queue(), ^{
                        [parent sendMessage:@"save-data-over" payload:message];
                    });
                parent.isDownloadFile = false;
                [self cleanDataSaveParameter];
                //[parent stop];
            }
        });
}
@end

@implementation PeerConnectionParameters
@end

@implementation webrtcClient {
    id<RecoLiveEventDelegate> delegate;
    SRWebSocket* webSocket;
    RTCPeerConnection* pc;
    RTCPeerConnectionFactory* factory;
    PeerConnectionParameters* peerConnectionParameters;
    RTCMediaConstraints* constraints;
    BOOL loginInSuccess;
    NSString* userName;
    NSString* peerName;
    Peer* peer;
    NSString* turn_key;
    NSString* host;
    BOOL createWebrtcCalled;

    NSMutableArray *iceServersInternal;
    RTCEAGLVideoView *remoteView;
    NSLayoutConstraint *remoteViewTopConstraint;
    NSLayoutConstraint *remoteViewRightConstraint;
    NSLayoutConstraint *remoteViewLeftConstraint;
    NSLayoutConstraint *remoteViewBottomConstraint;
    RTCVideoTrack *remoteVideoTrack;
    
    dispatch_queue_t queuePingTask;
}

@synthesize dataChannelLocal = _dataChannelLocal;

- (BOOL)hasCallInit {return createWebrtcCalled;}

- (BOOL)isConnected {
    if (webSocket != NULL && webSocket.readyState == SR_OPEN) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)isLogined {
    return loginInSuccess;
}

- (instancetype)initWithDelegate:(id<RecoLiveEventDelegate>)delegate_ {
    if (self = [super init]) {
        delegate = delegate_;
        webSocket = nil;
        loginInSuccess = NO;
        turn_key = @"t=u408021891333";
        _isDownloadFile = NO;
        createWebrtcCalled = NO;
        queuePingTask = dispatch_queue_create("webrtc.PingTaskQueue", DISPATCH_QUEUE_SERIAL);
    }
    return self;
}

- (void)initWebsocket:(NSString* )socketAddress hostIP:(NSString* )hostIP peerName:(NSString* )peerNameString{
    if (loginInSuccess){return;}
    peerName = peerNameString;
    host = hostIP;
    webSocket = [[SRWebSocket alloc] initWithURLRequest:
        [NSURLRequest requestWithURL:[NSURL URLWithString:socketAddress]]];
    //[webSocket setDelegate:self];
    webSocket.delegate = self;
    [webSocket open];
    int timeout = 0;
    while(![self isConnected] && timeout < 12) {
        [NSThread sleepForTimeInterval:0.5f];
        timeout += 1;
    }
    if([self isConnected]) {
        if (!loginInSuccess){
            [self sendLoginSignal];
        } else {
            NSLog(@"logined alreadly");
        }
    } else {
        NSLog(@"webSocket have not connected");
        return;
    }
}

- (void)uninitWebsocket {
    if (!loginInSuccess){return;}
    loginInSuccess = NO;
    //mRecoLiveEvent.onLogout();
    if(peer != nil) {
        [peer cleanDataSaveParameter];
    }
    if(webSocket != nil) {
        [webSocket close];
        webSocket = nil;
    }
}

- (void)startWebrtc:(PeerConnectionParameters* )params peerName:(NSString* )peerNameString {
    if(!loginInSuccess) {
        NSLog(@"webSocket have not connected4");
        //mRecoLiveEvent.onLogout();
        return;
    }
    peerName = peerNameString;
    createWebrtcCalled = YES;
    peerConnectionParameters = params;
    factory = [[RTCPeerConnectionFactory alloc] init];
    [self addPeer];
    NSLog(@"start:%d", params.useVideoChannel);
    peer.videoChannelUsed = params.useVideoChannel;

    NSLog(@"currentThread: %@", [NSThread currentThread]);
}

- (void)stopWebrtc {
    if(!createWebrtcCalled) { return; }
    createWebrtcCalled = NO;
    _isDownloadFile = NO;
    [peer cleanDataSaveParameter];
    if( _dataChannelLocal != nil ) {
        _dataChannelLocal = nil;
    }
    if (pc != nil) {
        [pc close];
        pc = nil;
    }
    peer = nil;
    if (factory != nil) {
        factory = nil;
    }
    constraints = nil;
    iceServersInternal = nil;
}

- (void)sendMessage:(NSString* )type payload:(NSMutableDictionary* )payload {
    if (webSocket.readyState == SR_OPEN) {
        NSLog(@"currentThread: %@", [NSThread currentThread]);
        NSError *error = nil;
        NSMutableDictionary* message = [NSMutableDictionary dictionaryWithCapacity:3];
        [message setObject:payload forKey:@"payload"];
        [message setObject:peerName forKey:@"name"];
        [message setObject:type forKey:@"type"];
        NSData *data = [NSJSONSerialization dataWithJSONObject:message
                                                       options:kNilOptions
                                                         error:&error];
        if(error ){
            NSLog(@"sendMessage ERROR: %@", error);
            return;
        }
        NSString* messageString =[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        [webSocket send:messageString];
    } else {
        NSLog(@"WebSocket not opened");
    }
}

- (void)sendMessage:(NSString* )message {
    if (webSocket.readyState == SR_OPEN) {
        NSLog(@"currentThread: %@", [NSThread currentThread]);
        NSString* jsonString =
            [NSString stringWithFormat:
                           @"{\"type\": \"message\", \"payload\": \"%@\", \"name\": \"%@\"}", message, peerName];
        [webSocket send:jsonString];
    } else {
        NSLog(@"WebSocket not opened");
    }
}

- (NSInteger) requestGetFile:(NSString* )fileName savePath:(NSString* )savePath {
    if(_isDownloadFile) {
        return -1;
    }
    _isDownloadFile = YES;
    peer.savePath = savePath;
    [peer cleanDataSaveParameter];
    NSMutableDictionary* payload = [NSMutableDictionary dictionaryWithCapacity:3];
    NSLog(@"%@: %@", savePath, fileName);
    [payload setObject:fileName forKey:@"fileName"];
    NSLog(@"%@: %@", savePath, fileName);
    [self sendMessage:@"file-transform" payload:payload];
    return 0;
};

- (void)cancelFileDownload {
    NSLog(@"cancelFileDownload");
    if(!_isDownloadFile) {
        return;
    } else {
        _isDownloadFile = NO;
        [peer cleanDataSaveParameter];
        [delegate onFileAbort:@"取消文件传输"];
        //mRecoLiveEvent.onFileAbort();
        NSMutableDictionary* payload = [NSMutableDictionary dictionaryWithCapacity:3];
        [self sendMessage:@"file-transform-cancel" payload:payload];
        return;
    }
}

- (void)sendLoginSignal {
    unsigned int n = arc4random();
    userName = [NSString stringWithFormat:@"%u", n];
    NSString* jsonString = [NSString stringWithFormat:@"{\"type\": \"login\", \"name\": \"%@\"}", userName];
    NSLog(@"currentThread: %@", [NSThread currentThread]);
    [webSocket send:jsonString];
}

- (long)getTimeStamp:(NSString* )host_add{
    NSURL *url = [[NSURL alloc] initWithString:host_add];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url];
    NSError* error = nil;
    NSURLResponse  *response = nil;
    NSData *received = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(error){
        NSLog(@"getTimeStamp ERROR: %@", error);
        long recordTime = [[NSDate date] timeIntervalSince1970]*1000;
        return recordTime;
    } else {
        NSString *str = [[NSString alloc]initWithData:received encoding:NSUTF8StringEncoding];
        NSLog(@"%@",str);
        NSString* timeStampLable = @"timestamp_is_";
        NSRange rangeStart = [str rangeOfString:timeStampLable];
        NSString* timeStampString = [str substringWithRange:NSMakeRange(NSMaxRange(rangeStart), str.length-NSMaxRange(rangeStart))];
        NSLog(@"%@",timeStampString);
        return [timeStampString longLongValue];
    }
}

- (NSString* )hmacSha1Encrypt:(NSString* )coturnUserName key:(NSString* )key {
    const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [coturnUserName cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC
                                          length:sizeof(cHMAC)];
    return [HMAC base64EncodedStringWithOptions:0];
}

- (void)addPeer{
    peer = [[Peer alloc] initWithDelegate:delegate creator:self];
    [peer cleanDataSaveParameter];
    
    long time_to_live = 600;  //10 minutes;
    //sync timestamp
    long timeStamp = [self getTimeStamp:[NSString stringWithFormat:@"%@%@%@", @"http://", host, @":3033/timestamp"]];
    NSString* timeStampLive = [NSString stringWithFormat:@"%ld", timeStamp + time_to_live];
    NSString* coturnUserName = [NSString stringWithFormat:@"%@:%@", timeStampLive, userName];
    NSString* password = [self hmacSha1Encrypt:coturnUserName key:turn_key];
    //NSLog(@"%@%@%@", coturnUserName, turn_key, password);
    NSString* server_url = [NSString stringWithFormat:@"turn:%@", host];
    iceServersInternal = [
        NSMutableArray arrayWithObject: [[RTCIceServer alloc] initWithURLStrings:@[
                [NSString stringWithFormat:@"%@%@", server_url, @":3478?transport=udp"],
                [NSString stringWithFormat:@"%@%@", server_url, @":3478?transport=tcp"],
                [NSString stringWithFormat:@"%@%@", server_url, @":3479?transport=udp"],
                [NSString stringWithFormat:@"%@%@", server_url, @":3479?transport=tcp"]]
                                                                        username:coturnUserName
                                                                      credential:password]];
    RTCConfiguration *config = [[RTCConfiguration alloc] init];
    config.iceServers = iceServersInternal;
    NSDictionary *mandatoryConstraints = @{
        @"OfferToReceiveAudio" : @"false",
        @"OfferToReceiveVideo" : @"true"
    };
    NSDictionary *optionalConstraints = @{
        @"DtlsSrtpKeyAgreement" : @"true"
    };
    constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints
                                                        optionalConstraints:optionalConstraints];
    pc = [factory peerConnectionWithConfiguration:config
                                   constraints:constraints
                                      delegate:peer];
    RTCDataChannelConfiguration *configuration = [[RTCDataChannelConfiguration alloc] init];
    configuration.isOrdered = YES;
    configuration.channelId = 1;
    _dataChannelLocal = [pc dataChannelForLabel:@"data_channel" configuration:configuration];
    _dataChannelLocal.delegate = peer;
}

- (void)handleLogin:(NSDictionary* )message {
    BOOL login_result = [[message objectForKey:@"success"] boolValue];
    if(login_result){
        loginInSuccess = YES;
        [delegate onLogin];
        [delegate onConnect];
        dispatch_async(queuePingTask, ^{
                while(loginInSuccess) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                            NSString* jsonString = [NSString stringWithFormat:@"{\"type\": \"ping-signal\"}"];
                            NSLog(@"currentThread: %@", [NSThread currentThread]);
                            [webSocket send:jsonString];
                        });
                    [NSThread sleepForTimeInterval:10];
                }
                NSLog(@"send ping exit");
            });
        NSLog(@"Login success");
    } else{
        NSLog(@"Ooops...try a different username");
        [self sendLoginSignal];
    }
}

- (NSString* )preferCodec:(NSString* )sdpString preferredVideoCodec:(NSString *)codec {
    NSString *lineSeparator = @"\n";
    NSString *mLineSeparator = @" ";
    // Copied from PeerConnectionClient.java.
    // TODO(tkchin): Move this to a shared C++ file.
    NSMutableArray *lines =
        [NSMutableArray arrayWithArray:
                            [sdpString componentsSeparatedByString:lineSeparator]];
    NSInteger mLineIndex = -1;
    NSString *codecRtpMap = nil;
    // a=rtpmap:<payload type> <encoding name>/<clock rate>
    // [/<encoding parameters>]
    NSString *pattern = [NSString stringWithFormat:@"^a=rtpmap:(\\d+) %@(/\\d+)+[\r]?$", codec];
    NSRegularExpression *regex =
        [NSRegularExpression regularExpressionWithPattern:pattern
                                                  options:0
                                                    error:nil];
    for (NSInteger i = 0; (i < lines.count) && (mLineIndex == -1 || !codecRtpMap); ++i) {
        NSString *line = lines[i];
        if ([line hasPrefix:@"m=video"]) {
            mLineIndex = i;
            continue;
        }
        NSTextCheckingResult *codecMatches =
            [regex firstMatchInString:line options:0 range:NSMakeRange(0, line.length)];
        if (codecMatches) {
            codecRtpMap =
                [line substringWithRange:[codecMatches rangeAtIndex:1]];
            continue;
        }
    }
    if (mLineIndex == -1) {
        NSLog(@"No m=video line, so can't prefer %@", codec);
        return @"";
    }
    if (!codecRtpMap) {
        NSLog(@"No rtpmap for %@", codec);
        return @"";
    }
    NSArray *origMLineParts = [lines[mLineIndex] componentsSeparatedByString:mLineSeparator];
    if (origMLineParts.count > 3) {
        NSMutableArray *newMLineParts =
            [NSMutableArray arrayWithCapacity:origMLineParts.count];
        NSInteger origPartIndex = 0;
        // Format is: m=<media> <port> <proto> <fmt> ...
        [newMLineParts addObject:origMLineParts[origPartIndex++]];
        [newMLineParts addObject:origMLineParts[origPartIndex++]];
        [newMLineParts addObject:origMLineParts[origPartIndex++]];
        [newMLineParts addObject:codecRtpMap];
        for (; origPartIndex < origMLineParts.count; ++origPartIndex) {
            if (![codecRtpMap isEqualToString:origMLineParts[origPartIndex]]) {
                [newMLineParts addObject:origMLineParts[origPartIndex]];
            }
        }
        NSString *newMLine = [newMLineParts componentsJoinedByString:mLineSeparator];
        [lines replaceObjectAtIndex:mLineIndex withObject:newMLine];
    } else {
        NSLog(@"Wrong SDP media description format: %@", lines[mLineIndex]);
    }
    NSString *mangledSdpString = [lines componentsJoinedByString:lineSeparator];
    return mangledSdpString;
}

- (void) didCreateSessionDescription:(RTCSessionDescription *)sdp
                               error:(NSError *)error {
  __weak webrtcClient *weakSelf = self;
  dispatch_async(dispatch_get_main_queue(), ^{
    if (error) {
      NSLog(@"Failed to create session description. Error: %@", error);
      [self stopWebrtc];
      return;
    }
    // Prefer H264 if available.
    NSString* sdpString = [self preferCodec:sdp.sdp preferredVideoCodec:@"H264"];
    if([sdpString isEqualToString:@""]) {
        NSLog(@"Failed to preferredVideoCodec");
        [delegate onDisconnected];
    }
    RTCSessionDescription *sdpPreferringH264 = [[RTCSessionDescription alloc] initWithType:sdp.type
                                                                                       sdp:sdpString];
    [pc setLocalDescription:sdpPreferringH264
                       completionHandler:^(NSError *error) {
            if (error) {
                NSLog(@"Failed to setLocalDescription Error: %@", error);
                [weakSelf stopWebrtc];
                return;
            } else {
                NSLog(@"set session description success");
            }
        }];
    NSMutableDictionary* payload = [NSMutableDictionary dictionaryWithCapacity:3];
    [payload setObject:[RTCSessionDescription stringForType:sdp.type] forKey:@"type"];
    [payload setObject:sdpPreferringH264.sdp forKey:@"sdp"];
    [self sendMessage:[RTCSessionDescription stringForType:sdp.type] payload:payload];
  });
}

- (void)handleOffer:(NSString* )from payload:(NSDictionary* )payload {
    dispatch_async(dispatch_get_main_queue(), ^{
            RTCSessionDescription* sdp =
                [[RTCSessionDescription alloc]
                    initWithType:[RTCSessionDescription typeForString:[payload objectForKey:@"type"]]
                             sdp:[payload objectForKey:@"sdp"]];
            [pc setRemoteDescription:sdp
                                completionHandler:^(NSError *error) {
                    if(error) {
                        NSLog(@"Failed to setRemoteDescription Error: %@", error);
                    } else {
                        NSLog(@"success to setRemoteDescription");
                    }
                }];
            [pc answerForConstraints:constraints
                                completionHandler:^(RTCSessionDescription *sdp,
                                                    NSError *error) {
                    [self didCreateSessionDescription:sdp
                                                error:error];
                }];
        });
}
- (void)handleAnswer:(NSString* )from payload:(NSDictionary* )payload {
}
- (void)handleCandidate:(NSString* )from payload:(NSDictionary* )payload {
    if (pc.remoteDescription != nil) {
        RTCIceCandidate* candidate = [[RTCIceCandidate alloc] initWithSdp:[payload objectForKey:@"candidate"]
                                                            sdpMLineIndex:[[payload objectForKey:@"sdpMLineIndex"] intValue]
                                                                   sdpMid:[payload objectForKey:@"sdpMid"]];
        [pc addIceCandidate:candidate];
    }
}

- (void)handleFileTransform:(NSString* )from payload:(NSDictionary* )payload {
    peer.fileName = [payload objectForKey:@"fileName"];
    peer.fileSize = [[payload objectForKey:@"fileSize"] intValue];
    peer.savePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSLog(@"savePath: %@", peer.savePath);

    [delegate onFileAccept];
    NSMutableDictionary* message = [NSMutableDictionary dictionaryWithCapacity:3];
    [self sendMessage:@"file-transform-parameter-accept" payload:message];
}

- (void)handleLeave {
    dispatch_async(dispatch_get_main_queue(), ^{
            [delegate onDisconnected];
        });
}

//SRWebSocketDelegate
- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)data {
    NSData* jsonData = [data dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary*  message = [NSJSONSerialization JSONObjectWithData:jsonData
                                                             options:NSJSONReadingMutableLeaves
                                                               error:nil];
    NSString* type = [message objectForKey:@"type"];
    //if(![type isEqualToString:@"candidate"] && ![type isEqualToString:@"pong-signal"]){
    NSLog(@"Websocket got message %@", data);

    NSString* from = [message objectForKey:@"from"];
    NSDictionary* payload = [message objectForKey:@"payload"];
    //NSLog(@"Websocket got message %d", from == nil);
    if(from != nil && ![from isEqualToString:@""] &&
       (peerName == nil || ![peerName isEqualToString:from])) {
        NSLog(@"peerName logic ERROR, peerName: %@ message from: %@", peerName, from);
    }
    if([type isEqualToString:@"login"]){
        [self handleLogin:message];
    } else if([type isEqualToString:@"offer"]){
        [self handleOffer:from payload:payload];
    } else if([type isEqualToString:@"answer"]){
        [self handleAnswer:from payload:payload];
    } else if([type isEqualToString:@"candidate"]){
        [self handleCandidate:from payload:payload];
    } else if([type isEqualToString:@"file-transform-parameter"]){
        [self handleFileTransform:from payload:payload];
    } else if([type isEqualToString:@"file-transform-file-size-zero"]){
        [delegate onFileSizeError:@"请求传输的文件大小为0"];
        _isDownloadFile = NO;
        [peer cleanDataSaveParameter];
    } else if([type isEqualToString:@"leave"]){
        [self handleLeave];
    } else if([type isEqualToString:@"message"]){
        [delegate onNotify:[NSString stringWithFormat:@"%@", payload]];
    } else if([type isEqualToString:@"pong-signal"]){
        ;
    } else if([type isEqualToString:@"peer-excced-3"]){
        [delegate onMessage:@"连接的用户超过3人,请稍后再试"];
    } else if([type isEqualToString:@"peer-not-exist"]){
        [delegate onMessage:@"设备不存在,请重新输入设备名"];
        [self stopWebrtc];
    } else {
        NSLog(@"unkonw message type");
    }
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    NSLog(@"Websocket Failed With Error %@", error);
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code
           reason:(NSString *)reason wasClean:(BOOL)wasClean {
    NSLog(@"WebSocket closed, code: %zd, reason %@, wasClean: %d", code, reason, wasClean);
}

@end
